Rebol [
    title: "Login functions to chat"
    file: %login.reb
    date: 24-Feb-2020
    notes: {send email address to login page, and gets a session key return, encrypted with your own public key
        call gpg to decrypt the key
    }
]

import <webform>

; chat-server: http://35.224.174.22

accounts: copy []

chat-account: make object! [
    site: _
    sessionkey: _
    username: _
    email: _
    room: _
]

; returns the chat-account object now
login: func [ chat-server [url!] username [word!] email [email!]
    /pass [text!]
    /debug
    <local> login-address payload response script sessionkey phrase chat-obj err
][
    chat-server: dirize chat-server
    if err: trap [
        call/output "gpg --help" false
    ][
        print "GPG does not appear to be installed or is not in the path."
        print "On Ubuntu, Debian, Mint and Kali, it's `sudo apt install gnupg`"
        print "On CentOS, Fedora, RHEL it's `sudo yum install gnupg`"
        print "On Windows, try downloading a binary from https://www.gnupg.org/(en)/download/index.html"
        print "On MacOs, use homebrew `brew install gnupg`"
        halt
    ]
    if pass [
        phrase: copy pass
    ]

    login-address: join chat-server "login"

    payload: join "email=" url-encode email 
    response: write login-address reduce ['POST payload] 
    
    response: to text! response
    if debug [
        probe response
    ]
    if parse response [ "userid:" any space copy userid numbers thru "sessionkey:" thru "-----BEGIN PGP MESSAGE-----" newline newline thru "-----END PGP MESSAGE-----"][
        if not pass [ 
            cycle [
                prin "Enter your GPG decryption phrase (Q=quit): "
                phrase: input
                if phrase = "Q" [halt]
                if 7 < length-of phrase [break]
            ]
        ]
    ] else [
        print response
        return
    ]
    if err: trap [
        write %challenge.gpg remove/part response find response "-----BEGIN PGP MESSAGE-----"
    ][
        if find mold err "Access is denied." [
            print "Can't write to file system.  Make sure you have write access"
            if system/version = 2.102.0.3.40 [
                print "Since you're on windows, make sure you started from within a shell"
            ]
        ]
        halt
    ]
    attempt [delete %challenge.txt]
    script: unspaced [{gpg --pinentry-mode=loopback --passphrase "} phrase {" -d -o challenge.txt challenge.gpg}] 
    call script
    if exists? %challenge.txt [
        response: to text! read %challenge.txt
        delete %challenge.txt
        if find accounts chat-server [remove/part find accounts chat-server 2]
        append accounts chat-server
        append accounts chat-obj: make chat-account compose [site: (chat-server) userid: (to integer! userid) sessionkey: (response) email: (email) username: (form username)]
        probe accounts
        chat-obj
    ] else [
        print "Failed to decrypt message"
    ]
]