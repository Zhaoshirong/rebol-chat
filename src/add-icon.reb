Rebol [
    title: "Add or update chat icon"
    file: %add-icon.reb
    date: 27-Feb-2020
    notes: {updates the icon used for chat}
]

add-icon: func [ chat-account [object!] iconurl [url!]
    <local> obj response
][
    if not exists? iconurl [
        print "The URL is invalid"   
    ] else [
        if get in chat-account 'site [
            website: dirize chat-account/site
            obj: make chat-account compose [iconurl: (iconurl)]
            response: write to url! unspaced [website "update-icon"] reduce ['POST (mold obj)]
            probe to text! response
        ] else [
            print "No chat-server specified yet.  Maybe you're not logged in?"
        ]
    ]
]
