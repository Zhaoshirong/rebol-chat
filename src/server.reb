Rebol [
    title: "Demo Usage"
    Notes: {
        check the request/action. If it matches a request of the web root, then deliver the index.html
        If you want to add cookies etc, then you'll have to currently modify the build-header function
        in the httpd
        
        0.0.28 log remote request ip/ports and use write-log function ( need to cycle logs as well )
        0.0.29 log malformed post headers
        0.0.30 see if a <pre/> is honoured
        0.0.31 add a visual delimiter
        0.0.32 add a robots.txt
        0.0.33 add a list of allowed pages, everything gives a 404
        0.0.36 wrap all txt files with <pre/>
        0.0.37 split off functions into library
        0.0.39 moved GET processing into library.reb
    }
    date: 6-Feb-2020
    version: 0.0.39
]

w*: :lib/write
r*: :lib/read
home: what-dir
started: now ; see what time of uptime we have

-- home

import <webform>
; import <httpd>
import https://raw.githubusercontent.com/gchiu/rebol-httpd/master/httpd.reb ; https://github.com/gchiu/rebol-httpd/blob/master/httpd.reb
initial-rooms: [room-1: 0 room-2: 0]

do %library.reb
do https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/src/speak.reb

; read gpg decryption phrase
secret: _
parse to text! read %../secretphrase.txt [thru "pass=" copy secret to [newline | end]]  ; store the decryption key outside the www pages
if nothing? secret [
    print "unable to read passphrase"
    halt
]
probe secret
print "got the decryption key from file system"

; read youtube api key
youtube-key: to text! read %../youtube-api.key

probe system/script/header/date
probe system/script/header/version
?? pwd

write-log: func [data [block!]][
    w*/append %logs/log.txt spaced data
]

if not exists? %logs/log.txt [w* %logs/log.txt ""]

if not exists? %userdata [mkdir %userdata]

if not exists? %robots.txt [
    w* %robots.txt unspaced ["User-agent: *" newline "Allow: /rooms/" newline "Disallow: /" newline]
]

; now load all the users
users: copy []
userobj: make object! [
    email: _
    userid: _
    username: _
    lastread: 0
    sessionkey: _
    date: _
    iconurl: _
]

if not exists? %maxmessageno.reb [
    save  %maxmessageno.reb maxmessageno: 0
] else [
     maxmessageno: load  %maxmessageno.reb
]

-- maxmessageno

last-userno: 0
number: charset [#"0" - #"9"]
numbers: [some number]

use [ck data ur lr][
    for-each file read %userdata/ [
        if not dir? file [
            data: to text! read join %userdata/ file
            url: _
            if parse data [thru "email:" any space copy email to newline (trim email)][
                if parse data [thru "username:" any space copy username to newline (trim username)][
                    if parse data [thru "userid:" any space copy userid to newline (trim userid)][
                        if parse userid [numbers][
                            userid: to integer! userid
                            last-userno: max last-userno userid
                            parse data [thru "iconurl:" any space copy url [to newline | to end] (attempt [trim url])]
                            if not did parse url ["http" to end][
                                ck: form checksum/method to binary! email 'md5
                                url: remove/part ck 2
                                remove back tail url
                                lowercase url
                                url: join https://www.gravatar.com/avatar/ url
                            ]
                            if did parse data [thru "lastread:" any space "[" copy lr to "]" to end][
                                parse data [thru "lastread:" any space copy lr [to newline | to end]]
                                lr: do lr
                            ] else [lr: copy initial-rooms]
                            append users make userobj compose [email: (email) username: (username) userid: (userid) iconurl: (url) lastread: (lr)]
                        ]
                    ]
                ]
            ]
        ]
    ]
]
probe users

registered?: func [email [email!] users [block!]][
    for-each user users [
        -- user
        if user/email = email [ return user ]
    ]
    return false
]

webport: 80
srv-block: compose [
    scheme: 'httpd (webport)
]

process-requests: [
    -- request/action
    write-log [now/precise '| request/action '| request/remote-addr/remote-ip '| request/remote-addr/remote-port newline]
    switch request/method [
        "GET" [
            -- "got a get"
            -- request/action
            result: process-gets request
            -- result
            if block? result [
                ; status: 404 content: "text"
                response/status result/status
                response/content result/content
            ] else [
                render result
            ]
        ]
        
        "POST" [
            -- "got a post"         
            if err: trap [
                probe to text! request/binary
                probe request/target
                probe request/http-headers/content-length
            ][
                ; got a malformed POST 
                write-log [now/precise mold request/http-headers newline]
            ] else [
                case [
                    request/action = "POST /rooms" [render mold which-rooms]
                    
                    request/action = "POST /create-webspeak" [render webspeak request/binary request/request-uri]

                    request/action = "POST /register" [render validate-registration request/binary]
                    
                    request/action = "POST /login" [render form validate-login request/binary]
                    
                    request/action = "POST /update-icon" [render validate-update-icon request/binary]

                    parse request/action ["POST /rooms/" copy room-no numbers end] [render validate-speech request/binary to integer! room-no]
                    
                    request/action = "POST /whois" [render whois request/binary] ; returns list of names from gpg keychain
                    request/action = "POST /whoare" [render whoare-users request/binary] ; returns block of [userid username]
                    
                    request/action = "POST /read" [render read-chat request/binary]

                    default [response/status: 500 response/content: "don't understand your post"]
                ]
            ]        
        ]
        
        "PUT" [
            -- "got a put"
                response/status: 403
                response/content: "Not currrently enabled for HTTP PUT"
        
        ]
        "DELETE" [
            -- "got a delete"
                response/status: 403
                response/content: "Not currently enabled for HTTP DELETE"
        
        ]
    ]
    response/set-cookie: default [copy "Nothing here folks"]
]

insert/only tail srv-block process-requests

srv: open srv-block
    
print join "Serving on port " webport    
if err: trap [
    wait [srv]
][
    write-log [now/precise mold err newline]
]
