rebol [
	title: "Keep server running"
	date: 12-March-2020
	notes: {server keeps stopping for unknown reasons.  This runs ps and if the server isn't there it starts it up again
	    use: nohup /sbin/r3 keep.reb &
	}
]

if not exists? %episodes.txt [
    write %episodes.txt ""
]
cycle [
   if zero? ret: call/output "ps aux" %monitor.txt [
	; script ran okay
	result: to text! read %monitor.txt
	if not parse result [thru "/sbin/r3 server.reb" to end][
	    write/append %episodes.txt unspaced [newline now]
	   call* "./run.sh"
	] else [ print "running okay"]
	attempt [delete %monitor.txt]
   ]
   sleep 5
]
