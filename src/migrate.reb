Rebol [
    file: %migrate.reb
    title: "Migrate all the old chat messages in SO to current site"
    date: 5-Mar-2020
]

root: https://chat.stackoverflow.com/transcript/291/ ; 2010/10/30

if not exists?  %transcript/ [
    mkdir %transcript/
]

;current: 30-Oct-2010 ; when chat started
current: 4-Mar-2020 ; the day before we finished getting the daily transcripts for 10 years
; n: 0
curdir: what-dir

; while [current < now ][
cycle [
    ; sp: 7
    wp: 24:00
    current: me + 1
    ; n: me + 1
    ; -- n
    ; if n > 10 [halt]
    if not exists?  yearfolder: to file! unspaced [%transcript/ current/year "/"][
        mkdir yearfolder
    ]
    if not exists? monthfolder: to file! unspaced [yearfolder current/month "/"][
        mkdir/deep monthfolder
    ]
    dayfile: to file! unspaced [monthfolder current/day]
    -- dayfile
    if err: trap [
        if not exists? dayfile [
            page: unspaced [root current/year "/" current/month "/" current/day]
            -- page
            script: spaced ["wget" page]
            -- script
            -- monthfolder
            change-dir monthfolder
            -- "after changing folder"
            attempt [call script]
            change-dir curdir
;            if exists? page [
;                -- page
;                write dayfile read page
;            ]
        ] else [
            ; sp: 0
            wp: 0
        ]
    ][
        probe err
        ; halt
    ]
    ; sleep sp
    wait wp ; 24 hours for each transcript to arrive
    ; halt
]