Rebol [
    title: "Who are registered"
    file: %whoare.reb
    notes: {returns a list of all past and current registered users with their ids}
    date: 7-March-2020
]

whoare: func [ chat-account [object!]
    <local> response website
][
    if did all [
        website: dirize get in chat-account 'site 
        something? chat-account/sessionkey
    ][
        response: load/all to text! write to url! unspaced [website "whoare" ] reduce ['POST (mold chat-account)]
        return response
    ] else [
        print "You're not logged in yet"
    ]
]
