Rebol [
    title: "Read messages in current room"
    file: %read-chat.reb
    date: 1-Mar-2020
    notes: {function to read messages from current room}
]

 simple-date: func [d [date!]
    <local> ds day month year n
][
    n: now
    day: d/day
    year: d/year
    parse form d/6 [thru "-" copy month to "-"]
    ds: copy if d/date = n/date ["Today"] else [spaced [month day if year <> n/year [year]]]
    append ds unspaced [space d/hour ":" next form 100 + d/minute]
    ds
]

read-chat: func [chat-account [object!] roomno [integer!]
    /asobjects?
    <local> website obj response
][
    chat-account/room: roomno
    if not something? chat-account/sessionkey [ return "-ERR Not logged in"]

    if get in chat-account 'site [
        website: dirize chat-account/site
        response: write to url! unspaced [website "read"] reduce ['POST (mold chat-account)]
        response: load to text! response
        if asobjects? [
            return mold/all response
        ]
        if all [block? response object? response/1] [
            for-each msg response [
                print spaced [
                    "User:" msg/userid space 
                    "Msg:" msg/messageid attempt [if integer? msg/replyto [join "replying to: " msg/replyto]]
                    "on" unspaced [simple-date msg/date + now/zone space now/zone]
                ]
                print head remove back tail remove/part copy msg/content 1
                print newline
            ]
        ] else [
            print form response
        ]
        ;probe type-of response
        ;probe length-of response
    ] else [
        return "-ERR No chat-server specified yet.  Maybe you're not logged in?"
    ]
]
