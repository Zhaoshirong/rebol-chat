Rebol [
    file: %install.reb
    title: "Chat installer"
    date: 7-March-2020
    author: "Graham"
    notes: {
        0. makes sure gpg is setup
        1. saves the gpg password locally
        2. downloads the relevant server, library files
        3. queries the server for the number of rooms
        4. downloads all the chat into the various rooms
        5. starts serving the files locally on requested port
        6. polls server for new messages
    }
]

chat-server: http://35.224.174.22/

; check to see if gpg is installed

if trap [
    if not zero? ret: call/output "gpg --help" false [
        print spaced ["Need to install GPG properly.  A return value of" ret "was received"]
        halt
    ]
][
    print "You don't seem to have GPG installed."
    halt
]

; change back to the local current dir

cd (system/options/path)

; check we are in the right directory

if %chat/ <> last split-path what-dir [
    print "This script needs to run from a directory call `chat`"
    halt
]

; now to store the GPG password locally

if not exists? %secretphrase.txt [
    cycle [
        prin "We need the GPG passphrase to save locally (Q): "
        passphrase: input
        if passphrase = "Q" [halt]
        if not empty? passphrase [
            print "saving passphrase as secretphrase.txt"
            write %secretphrase.txt join "pass=" passphrase
            break
        ]
    ]
] else [
    print "secret phrase exists"
    passphrase: find/tail to text! read %secretphrase.txt "pass=" 
]

; youtube api key
if not exists? %youtube-api.key [
    cycle [
        prin "We need your Youtube api key to save locally (Q/NA): "
        youtube-key: input
        if youtube-key = "Q" [halt]
        if not empty? youtube-key [
            write %youtube-api.key youtube-key
            break
        ]
    ]
]

; let's make the various directories

directories: [%www/ %www/logs/ %www/rooms/ %www/transcript/]

for-each d directories [
    if not exists? d [
        print join "making " d
        mkdir/deep d
    ]
]

update-room-directories: func [rooms][
    ; now make the rooms based on server response
    for-each room rooms [
        if not exists? room: to file! unspaced [%www/rooms/ room "/"][
            print join "making " room
            mkdir/deep room
        ]
    ]
]

get-rooms: func [chat-server [url!]
    <local> err
][
    if not dir? chat-server [chat-server: dirize chat-server]
    ; and now make the rooms, getting them first
    if err: trap [
        print "checking which rooms are available"
        rooms: load to text! write join chat-server "rooms" [POST ""]
        return rooms
    ][
        print "Unable to load room information"
        probe err
        return copy []
    ]
]

update-messages: func [o [object!] rooms [block!]][
    for-each room rooms [
        print "reading room " room 
        messages: load/all read-chat/asobjects? o to integer! head remove back tail form room
        -- messages
        messages: messages/1
        -- messages
        room: join %rooms/ room
        -- room
        change-dir room
        if all [
            block? messages
            object? messages/1
        ][
            for-each message messages [
                ; -- message
                ; probe message
                print join "saving " message/messageid
                save to file! join next (form 100000000 + message/messageid) %.reb message
            ]
        ]
        change-dir home
    ]
]

update-users: func [o [object!] users [block!]
][
    ; now grab all the users and create userdata files for them
    users: whoare o
    for-each user users/1 [
        write join %userdata/ user/username spaced [
            "userid:" user/userid newline
            "username:" user/username newline
            "email:" user/email newline
            "iconurl:" user/iconurl newline
            "lastread:" "[]" newline
            "password:" {""} newline
            "full name:" {""} newline
        ]
    ]
]

; rooms: get-rooms chat-server
; update-room-directories rooms

; now download the server script
change-dir %www/
home: what-dir
if err: trap [
    print "Fetching server scripts"
    write %server.reb read https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/src/server.reb
    write %library.reb read https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/src/library.reb
][
    probe err
    halt
]

if not exists? %index.html [
    write %index.html read chat-server
]
if not exists? %favicon.ico [
    print "reading favicon.ico"
    write %favicon.ico read join chat-server %favicon.ico
]

; store credentials locally
if not exists? %credentials.reb [
    cycle [
        prin unspaced ["What is your username for " chat-server "(Q): "]
        username: input
        if username = "Q" [halt]
        prin unspaced [ "What is your email for " chat-server "(Q): "]
        email: input
        if email = "Q" [halt]
        -- email
        -- username
        if any [empty? username empty? email][continue] 
        save/all %credentials.reb make object! compose [website: (chat-server) username: (to 'word! username) email: (to email! email)]
        break
    ]
] else [
    credentials: load %credentials.reb
    credentials/username: to word! credentials/username
    credentials/email: to email! credentials/email
    for-each key [website email username][
        if not in credentials key [
            print join "missing " key
            halt
        ]
    ]
]

; login
print "loading all the chat utils ..."
do <chat>

cycle [
    prin "What server port to use? (Q) "
    webport: input 
    attempt [
        webport: load webport
    ]
    if integer? webport [break]
]

script: to text! read %server.reb
replace script "webport: 80" join "webport: " webport
write %myserver.reb script

chat-server: dirize chat-server
users: copy []
o: _

; try the one call to start the server
call* form compose [(form system/options/boot) "myserver.reb"]

cycle [
    if err: trap [
        if nothing? o [
            -- "logging in"
            -- chat-server
            -- passphrase
            probe credentials
            if err2: trap [
                o: login/pass chat-server credentials/username credentials/email passphrase
                ; the webcredentials are used by the webserver to post to the main server
                save/all %webcredentials.reb o
            ][
                print "Unable to login to server"
                probe err2
            ]
        ]
comment { ; this is leading to zombie processes 
        if err2: trap [
            read join to url! unspaced [http://localhost: webport %/index.html]
        ][
            --"Can't open the server port"
            probe err2
            
            -- "Opening server port"
            call* form compose [(form system/options/boot) "myserver.reb"]
        ]
}
        if object? o [
            -- "getting rooms"
            rooms: get-rooms chat-server
            -- "updating rooms"
            update-room-directories rooms
            -- "updating users"
            update-users o users
            -- "updating messages"
            update-messages o rooms
            ; should check for new users as well ... maybe in update-messages if new user posted
        ]
    ][
        print "unable to talk to server" 
        print mold err
        ; try logging in again
        o: _
    ]
    prin "."
    sleep 10
]

print "Finished"
