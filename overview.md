# Overview

The server is running Rebol httpd.reb.

A Rebol client goes to a code page and grabs the new functions for dealing with the chat system.

## Registration

C: Send their registration details eg. [sample](/users/graham.md) as [raw link](https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/users/graham.md)

S: Reads the raw page, and parses out the prospective user's public PGP key, and their encrypted registration details.  If all is well, then add user, and import their public key to chat-server's key store.  Reply with Ok message.

If something is wrong, reply to that effect

## Login

C: Sends their email address (associated with their public key) to the chat server

S: If finds their email, then encrypts a one time password using Client's public key.

C: decrypts the password, and then logs in.

S: sends back a session key as cookie which is used by client for the rest of the session

C: sends chat messages signed with their private key

## Problems

This idea is subject to a man in the middle attack since we're not using https.  So, another idea is that the server sends back an encrypted session key to be used by the client, and a list of one time codes to be used sequentially by the client for that session.