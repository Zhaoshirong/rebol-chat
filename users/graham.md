```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGNBF5SQT0BDACwp+FKaclGXNRhWvsSGaBn7dOfFlkk9n9ANjhIUAGXygz2IxIC
T1dBePk613LoT94ArTqBPUm5i8t3bFIoYl7XDLXEJ8oOXB5ONRsMm9/OJOZlu7Ev
w76o2mASJrln1kZNhFuxcGIWUE6OgKOUr4HergsSonWmZj7eNSiof++5n7DoR9X7
YKD5NB+MeGBRjoVbab+H/onqSRprt7VElGy/FXwGcm2NB+WEXk5VNiHaV8+2D2hX
fkqBtgMDMTJ4ZVhUky8I9z98B9rb1LF3QVmv/wO01Viu/GuMZYg2yGCoJw++Fe8+
cWuP0WEjBjs1Oip5shKo6PS7E/Irq4rv2wLHBaa9mXeyFTqIEo6Ak43vK1At58Od
uQovR9SY5eFHQN2X4CcnjRbc+ShNFx+gL5FjIuJYSpCzRVenSP78R3x9Ue7Zdz+v
BzE7gFQ3AZRgJEwwWJEpw6Bebpkz/zO+KwoayUWXOcS6yJr+MjSYbMppH67GZERR
YvFwqzOcrhkbXk0AEQEAAbQiR3JhaGFtIENoaXUgPGdyYWhhbUBjbG91ZC1laHIu
Y29tPokB1AQTAQoAPhYhBOKNq3x6p0YvOkXT6Qozl4qN/TiwBQJeUkE9AhsDBQkD
wmcABQsJCAcCBhUKCQgLAgQWAgMBAh4BAheAAAoJEAozl4qN/Tiw/C0L/1QXGB3V
XNQiomGih9xSrkdRjRM9Gfhg8DRIbi0ZVoSxm66meEB3yHULu2YkHovrO1M5tY5d
DaBqCW1DWwuRP2+Lh0tkpLDwupTfERs5r9/63kSS/hNUkGoXo5+Ve1t19Bb/Dt1W
7ui7CH6MQn4DKaTW7kbQuJqEYC8NVSMM0X4a4UPpqTN4OsJqODdMBSeytGSwBgrh
URqnmrahPI18sf0UuZ/xUIQKzhUloPb11KTI3LFQ9LLLnkEvc29lQLOLbdrSSve8
/BZjQl7BU0c7Vxu8qDhLNVFwHrSzDwOSKkVASjEselH65YZhc/xjGFUyhrtZYi1v
aGqoORR1JC2InBlY7IknOo2G4okXN4d/Vz0Hu5LillmiKGAz0lAP0DeB9ALiIDTl
TA01a2ERuBnlqnYflht0R7Mn3jLl1Q+u5mqZQeZK8fxWqqnQuHwHurvPbxIf3bbe
4wiF5qmDEneGHEiLzoucFU2Wcp77V3ACX9YIZW9Vl4xiwlL/c5dxGc1WzLkBjQRe
UkE9AQwAt7HTgVgm9PGmBa+uzHkkJpSRTjW/rjIF14ZvHkHm5mqD4xe/ZW4KltY4
Wtmon7AK2fMlwn79d5UwGJUjN8J7KMLuMSqdT2TMmlCMRSjXP382vcrDHT1FYL6K
S/e+yLuPW9eLSa97WrfMREla+XeqN7Aa1iAIY0WlkcDLzG7RzPXyQfn++jKT9lxI
WVTwy3BypCVKM+vkZBYrArafma5GHq1nlVq4uNvbBcJH98Bq9HXpq4IoLxb4hDX1
ImYc1gcLUn6In0Z0MoxrVINlNAaPOjXZm/KQHjmquPkv2Tw5anwhZfCClbOvrpS/
aXS53cHVJ8f6xsYLiSdpHMUl4rJNG43wkpFLYwRlb3ixl3t5ncIYBhI6UDQmcwxB
vPNJ+zBQe9KmI5YqniytuTqrVoJmOrjxC7V3aYRvx6L1TO4cMNA0HvnHS9p6+PF+
duPa7r8BDVrF9pT+5unbEzYFqRxX3KGRGDrbMIUp/ss/8DkZDbyxtZaHcdL2WJ+3
o3GHs/flABEBAAGJAbwEGAEKACYWIQTijat8eqdGLzpF0+kKM5eKjf04sAUCXlJB
PQIbDAUJA8JnAAAKCRAKM5eKjf04sDIoC/9dk1l6hE6s94Y2yd6YYKR5iEXH1HDL
rO1bcApxMHffz8TzfsHuUVotj6wsQOpqqLg5BwUwJ+1jB/broT1XixbfQHTDf/WL
AiCnAHRLqpPdVctKbC+dKr6uiPjOfEhRPTyRJFfuTNQ0C1iLi/CE0Rr41rT7MSbH
foRTnOYn3f+iZvUkVsrAa8oUeb0116tebU+LHQEEJZCcu1GGxLD9vyxHk5FCeq6Z
f0Ze9yFpODhzVVGfmzyswDW7/a5HYBapY8U9WTNEVO9fcyoq58ms7DBJiXStRCnT
iyvkgOOAW3iSzWmgVfvBUPGUmaBU2qFp39hIGvTuLdkoyBenfkLNuzh43uMr54/u
nf7wlg3jjAuYHzQWY9O93hdWUZYkYzH2NPveckCE8W0eK0jcuFjNlCKutu+TgN/p
8v6x59vQEu4MEb8CSGdqvF744aXNk2e8kosnQcirrL39HZA23OsXHAc5bv4bV5HH
boxSXsLhgT+ChbgkDBqjHVIXej6Xnp8Dx8c=
=kCaf
-----END PGP PUBLIC KEY BLOCK-----
```


```
-----BEGIN PGP MESSAGE-----

hQGMA+cGE45lRfRiAQv+L4vIz+08OVFCS8lp+nLMBcEjTu/dUdXAByqueIYCspzW
33bViVLvlWTPqExR3Q4oW9B9qXfQM5bvvwUg7Z9AzLLKJbEAfHHtc4XsmfDcNp7U
I1fMdK1Y9XvE0CUkEBz1j8NSxAJ/oHsfrYdr6MgXu03+qvpMJMZvXNdqZs948ZW2
YeLIqptfzU91ml0efEIGqlLjV28nLCEsWtxkC6D+RuPX485uGhydtus7dUWS1yfU
4ztcNwnEYFTCvuT9BrGFT7rL4RiWMxcLXcdY1A0fe6NoNJ1jSbeZAGlr4z8390F1
fsb4PfpmB+HwPGHftRidK2m7tbYW/wTV0nhVZZuaFtqValjAUf3QNLXY49UorjlY
hzAXO5mgDwOs5uE9Eiy6wq57vS1fMEXa28ik3ZLRnjX81L0YkyDhfiuFtrZ4z5hh
l0bLNHVQPCc9kZt/CoprRQo7P7PKzCLDzlc9dP9w3cgNUJDpBGMLUesev5dMF488
nLfx9kA3ELGjcEaFP4Tk0pYBJ61oneDixuATJBVKJY9XmtYXQRTJwCs4tEXf12ne
d0UBmjnDNQL18dAP8PhV0apIC/5Ik11k7gwEO8xOkYxIZEnx1kABCIlZTtbAyB9o
8wHs/kAPHiDBVhuTI0/IMs4duagVwgBU82pBjAc+QIQEf5Mn8tSh/VfjzoSBax4Z
Cuj5mWUNHSHd2oo8atwOrE7udqCJaEk=
=k7FS
-----END PGP MESSAGE-----
